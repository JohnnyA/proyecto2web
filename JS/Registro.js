
const USUARIOSKEY = 'usuarios';
function insertUsuario() {
	const nombre = document.getElementById('nombre').value;
    const apellido = document.getElementById('apellido').value;
    const telefono = document.getElementById('telefono').value;
    const nombre_usuario = document.getElementById('nombre_usuario').value;
    const contrasena = document.getElementById('contrasena').value;
    const contrasena2 = document.getElementById('contrasena').value;

	let currentKey = localStorage.getItem('usuariosLastInsertedId');

	if (!currentKey) {
		localStorage.setItem('usuariosLastInsertedId', 1);
		currentKey = 1;
	} else {
		currentKey = parseInt(currentKey) + 1;
		localStorage.setItem('usuariosLastInsertedId', currentKey);
	}

	// create the author object
	const usuario = {
		nombre,
		apellido,
        telefono,
        nombre_usuario,
        contrasena,
        contrasena2,
		id: currentKey
	};

	// add it to the database
	let usuarios = JSON.parse(localStorage.getItem(USUARIOSKEY));
	if (usuarios && usuarios.length > 0) {
		usuarios.push(usuario);
	} else {
		usuarios = [];
		usuarios.push(usuario);
	}
	localStorage.setItem(USUARIOSKEY, JSON.stringify(usuarios));

}
function clearFields() {
	document.getElementById('nombre').value = '';
    document.getElementById('apellido').value = '';
	document.getElementById('telefono').value = '';
	document.getElementById("nombre_usuario").value = '';
	document.getElementById("contrasena").value = '';
	document.getElementById("contrasena").value = '';
}
function bindEvents() {
	jQuery('#add-usuario-button').bind('click', function (element) {
		insertUsuario();
	});
}